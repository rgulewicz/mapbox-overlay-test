# Census block map demo

A quick & dirty test of a choropleth map that shows census tracts in the state of Kentucky. It uses a custom shape layer displayed over a Mapbox map. The data source for the shapes is a GeoJSON file converted from a shapefile.

## How to run

1. Run `npm i`
2. Rename `/.env.dist` to `.env`
3. Open `/.env` and insert a valid Mapbox token
4. Run `npm start`