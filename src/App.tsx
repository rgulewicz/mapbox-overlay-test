import React from 'react';
import './App.css';

import ChoroplethMap from './ChoroplethMap';

function App() {
  return (
    <div className="App">
      <ChoroplethMap/>
    </div>
  );
}

export default App;
