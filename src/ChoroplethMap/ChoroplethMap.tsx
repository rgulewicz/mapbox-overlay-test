import React from 'react';
import mapboxgl from 'mapbox-gl';

import './styles.css';
import data from './data';

mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_ACCESS_TOKEN as string;

type State = {
    lng: number,
    lat: number,
    zoom: number,
    hoveredFeatureId: number | string | null,
    tractName: string,
    show3d: boolean
}

class ChoroplethMap extends React.Component<{}, State> {

    mapContainer: HTMLElement;
    map: mapboxgl.Map;

    constructor(props: {}) {
        super(props);
        this.state = {
            lng: -85.3364,
            lat: 37.4500,
            zoom: 6,
            hoveredFeatureId: null,
            tractName: '',
            show3d: false
        };
    }

    componentDidMount() {
        const map = new mapboxgl.Map({
            container: this.mapContainer,
            style: 'mapbox://styles/mapbox/light-v10',
            center: [this.state.lng, this.state.lat],
            zoom: this.state.zoom
        });
        this.map = map;

        map.on('move', () => {
            this.setState({
                lng: +map.getCenter().lng.toFixed(4),
                lat: +map.getCenter().lat.toFixed(4),
                zoom: +map.getZoom().toFixed(2)
            });
        });

        map.on('load', () => {
            const layers = map.getStyle().layers;
            let firstSymbolId: string;
            for (var i = 0; i < layers.length; i++) {
                if (layers[i].type === 'symbol') {
                    firstSymbolId = layers[i].id;
                    break;
                }
            }

            map.addSource('kentucky-census-tracts', {
                'type': 'geojson',
                'generateId': true,
                'data': data
            });
                 
            map.addLayer({
                'id': 'census-tract-chloropeth-3d',
                'type': 'fill-extrusion',
                'source': 'kentucky-census-tracts',
                'layout': {
                    'visibility': 'none'
                },
                'paint': {
                    'fill-extrusion-height': {
                        property: 'someVeryImportantValue',
                        stops: [[0, 0], [1, 10000]]
                    },
                    'fill-extrusion-opacity': 0.5,
                    'fill-extrusion-color': [
                        'case',
                        [
                            'boolean',
                            ['feature-state', 'hover'],
                            false
                        ],
                        '#FF5733',
                        [
                            'interpolate', ['linear'],
                            ['get', 'someVeryImportantValue'],
                            0, '#fff',
                            1, '#239B56'
                        ]
                    ]
                },
                'filter': ['==', '$type', 'Polygon']
            }, firstSymbolId);

            map.addLayer({
                'id': 'census-tract-chloropeth-2d',
                'type': 'fill',
                'source': 'kentucky-census-tracts',
                'paint': {
                    'fill-color': [
                        'case',
                        [
                            'boolean',
                            ['feature-state', 'hover'],
                            false
                        ],
                        '#FF5733',
                        '#239B56'
                    ],
                    'fill-opacity': [
                        'case',
                        [
                            'boolean',
                            ['feature-state', 'hover'],
                            false
                        ],
                        0.6,
                        [
                            'interpolate', ['linear'],
                            ['get', 'someVeryImportantValue'],
                            0, 0,
                            1, 0.6
                        ],
                    ]
                },
                'filter': ['==', '$type', 'Polygon']
            }, firstSymbolId);
        });

        

        map.on('mousemove', 'census-tract-chloropeth-2d', this.onTractMouseMove(map));
        map.on('mousemove', 'census-tract-chloropeth-3d', this.onTractMouseMove(map));

        map.on('mouseout', 'census-tract-chloropeth-2d', this.onTractMouseOut(map));
        map.on('mouseout', 'census-tract-chloropeth-3d', this.onTractMouseOut(map));
    }

    onTractMouseMove = (map: mapboxgl.Map) => (e: mapboxgl.MapLayerMouseEvent) => {
        map.setFeatureState({
            source: 'kentucky-census-tracts',
            id: this.state.hoveredFeatureId,
        }, {
            hover: false
        });

        const hoveredFeatureId = e.features[0].id;

        map.setFeatureState({
            source: 'kentucky-census-tracts',
            id: hoveredFeatureId,
        }, {
            hover: true
        });

        this.setState({
            tractName: e.features[0].properties.NAMELSAD,
            hoveredFeatureId
        });
    }

    onTractMouseOut = (map: mapboxgl.Map) => (e: mapboxgl.MapLayerMouseEvent) => {
        map.setFeatureState({
            source: 'kentucky-census-tracts',
            id: this.state.hoveredFeatureId,
        }, {
            hover: false
        });

        this.setState({
            tractName: '',
            hoveredFeatureId: null
        });
    }

    toggleLayer = () => {
        this.setState(
            prev => ({ show3d: !prev.show3d }),
            () => {
                if (this.state.show3d) {
                    this.map.setLayoutProperty('census-tract-chloropeth-3d', 'visibility', 'visible');
                    this.map.setLayoutProperty('census-tract-chloropeth-2d', 'visibility', 'none');
                } else {
                    this.map.setLayoutProperty('census-tract-chloropeth-3d', 'visibility', 'none');
                    this.map.setLayoutProperty('census-tract-chloropeth-2d', 'visibility', 'visible');     
                }
            }
        )
    }

    render() {
        const {
            lng,
            lat,
            zoom,
            tractName,
            show3d
        } = this.state;

        return (
            <div>
                <div className='sidebarStyle'>
                    <div>
                        Longitude: {lng} | Latitude: {lat} | Zoom: {zoom} {tractName ? ` | ${tractName}` : ''}    
                    </div>
                </div>
                <button
                    className="toggleLayer"
                    onClick={this.toggleLayer}
                >
                    {show3d ? '3D' : '2D'}
                </button>
                <div
                    ref={el => this.mapContainer = el}
                    className='mapContainer'
                />
            </div>
        )
    }
}

export default ChoroplethMap;
