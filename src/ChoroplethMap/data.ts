// import geoJsonData from './dataSimplified.json';
import geoJsonData from './data.json';

const enrichedData = geoJsonData as GeoJSON.FeatureCollection;

enrichedData.features.forEach(feature => {
    feature.properties.someVeryImportantValue = Math.random();
});

export default enrichedData;
